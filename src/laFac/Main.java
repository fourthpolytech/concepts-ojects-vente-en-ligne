package laFac;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Observer;
import java.util.Scanner;

public class Main {

	public static Client client;

	public static void main(String[] args) {

		client = creerClient();

		System.out.println("*************************************************");
		System.out.println("*************** BIENVENUE A LAFAC ***************");
		System.out.println("*************************************************");
		afficheMenu();

		Scanner sc = new Scanner(System.in);

		int choix = 1;
		while (choix != 0) {
			System.out.println("Votre choix : ");
			choix = sc.nextInt();

			switch (choix) {
			case 0:
				System.out.println("A bientot");
				choix = 0;
				break;
			case 1:

				Scanner scNom = new Scanner(System.in);
				try {
					inscription(scNom);
				} catch (IllegalArgumentException | InputMismatchException e) {
					System.out.println(e.getMessage());
				}
				break;
			case 2:
				Scanner scLog = new Scanner(System.in);
				connecter(scLog);
				break;
			case 3:
				afficheProdui();
				break;
			case 4:
				Scanner scN = new Scanner(System.in);
				ajoutProd(scN);
				break;
			case 5:
				client.payer();
				imprimeTicket();
				client.notifyObservers();
				choix = 0;
				break;
			case 6:
				try {
					client.seDeconnecter();
				} catch (ErreurStatut e) {
					System.out.println(e.getMessage());
				}
				break;
			case 7:
				System.out.println("voir le panier");
				affichePanier();
				break;
			case 8:
				afficheMenu();
				break;
			default:
				break;
			}

		}

	}

	// la liste des produits en vente
	public static ArrayList<Produit> listProduit() {
		ArrayList<Produit> list = new ArrayList<Produit>();

		list.add((new Tele("tv", 400.d, 20)));
		list.add(new Livre("Harry Poter", 50, 10, "J K Roling"));
		list.add(new Livre("L'alchimiste", 10, 3, "Paulo"));
		list.add(new Spectacle("Cirque du soleil", 45, 4, "TotemT"));
		list.add(new Spectacle("L'age des heros", 24, 2, "Marvel"));

		return list;
	}

	// la liste des offres flash de cette periode
	public static ArrayList<OffreFlash> listOffreFlash() throws ErreurProdNonOffrable {
		ArrayList<OffreFlash> list = new ArrayList<OffreFlash>();

		ArrayList<Produit> listP = new ArrayList<>();
		listP.add(new Livre("Harry Poter", 50, 10, "J K Roling"));
		listP.add(new Livre("L'alchimiste", 10, 3, "Paulo"));

		// of1 est une offre flash sur 2 livres
		OffreFlash of1 = new OffreFlash(0.15d, listP);

		// of1 est une offre flash sur 1 livre et un spectacle
		OffreFlash of2 = new OffreFlash(0.10d);
		of2.pdtConcerne.add(new Spectacle("Cirque du soleil", 45, 4, "TotemT"));
		of2.pdtConcerne.add(new Livre("Harry Poter", 50, 10, "J K Roling"));

		list.add(of1);
		list.add(of2);

		// list contient deux types d'offre flash
		return list;
	}

	// list des offres de produits
	public static ArrayList<OffreProduit> listOffreProduit() throws ErreurProdNonOffrable {
		ArrayList<OffreProduit> list = new ArrayList<OffreProduit>();

		OffreProduit op1 = new OffreProduit(0.1d, new Tele("tv", 400.d, 20));

		list.add(op1);

		// une seule offre de produit, qui est sur la tele
		return list;
	}

	// liste des offres adherant
	public static ArrayList<OffreAdherent> listOffreAdherant() throws ErreurProdNonOffrable {
		ArrayList<OffreAdherent> list = new ArrayList<OffreAdherent>();

		OffreAdherent oa = new OffreAdherent(0.35d, new Spectacle("Cirque du soleil", 45, 4, "TotemT"));
		oa.pdtConcerne.add(new Livre("L'alchimiste", 10, 3, "Paulo"));

		list.add(oa);

		return list;
	}

	// list des offres pour les membres
	public static ArrayList<OffreMembre> listOffreMembre() throws ErreurProdNonOffrable {
		ArrayList<OffreMembre> list = new ArrayList<OffreMembre>();

		OffreMembre om = new OffreMembre(0.10d, new Livre("Harry Poter", 50, 10, "J K Roling"));
		om.pdtConcerne.add(new Spectacle("Cirque du soleil", 45, 4, "TotemT"));
		om.pdtConcerne.add(new Spectacle("L'age des h�ros", 24, 2, "Marvel"));

		list.add(om);

		return list;
	}

	// list des observer
	public static ArrayList<Observer> listOberver() {
		ArrayList<Observer> listOb = new ArrayList<>();

		listOb.add(new AlerteProd());
		listOb.add(new AlerteCombinProd());
		listOb.add(new AlerteTotal(200d));

		return listOb;
	}

	// creer un client avec la liste des statuts qui se trouvent dans la base
	public static Client creerClient() {
		Client c = new Client();

		// les statuts de la base
		MembrePersonnel sPAnne = new MembrePersonnel("anneperso");
		Adherent sAAnne = new Adherent("anneadherent");
		MembrePersonnel sPFred = new MembrePersonnel("fredperso");

		ArrayList<CarteDeFidelite> sesCartes = new ArrayList<>();
		CarteDeFidelite c1 = new CarteDeFidelite(15);
		sesCartes.add(c1);
		sAAnne.setSesCartes(sesCartes);

		try {
			Client.addClient(sPAnne);
			Client.addClient(sPFred);
			Client.addClient(sAAnne);
		} catch (ErreurStatut e1) {
			System.out.println(e1.getMessage());
		}

		ArrayList<OffreFlash> offreF = new ArrayList<>();
		ArrayList<OffreProduit> offreP = new ArrayList<>();
		ArrayList<OffreMembre> offreM = new ArrayList<>();
		ArrayList<OffreAdherent> offreAdh = new ArrayList<>();
		try {
			offreF = listOffreFlash();
			offreP = listOffreProduit();
			offreM = listOffreMembre();
			offreAdh = listOffreAdherant();
		} catch (ErreurProdNonOffrable e) {
			System.out.println(e.getMessage());
		}

		// relier les offres aux diff�rents types de statuts
		Statut.offreFlashs.addAll(offreF);
		Statut.offrePdts.addAll(offreP);
		Adherent.offreAdherents.addAll(offreAdh);
		MembrePersonnel.offreMembres.addAll(offreM);

		// les observer avec les clients
		ArrayList<Observer> listOb = listOberver();
		c.addObserver(listOb);

		return c;
	}

	// retourne le produit, dans la base, qui a le nom name
	public static Produit nameToProd(String name) throws IllegalArgumentException {

		for (Produit p : listProduit()) {
			if (p.id.equals(name))
				return p;
		}
		throw new IllegalArgumentException(name + " n'existe pas dans la base");

	}

	// recupere les donees du nouveau client qui s'inscrit en tant qu'adhérant
	public static void inscription(Scanner sc) throws IllegalArgumentException, InputMismatchException {
		System.out.println("donnez votre login : ");
		String nom = null;
		nom = sc.nextLine();
		if (nom.equals("") || nom == null)
			throw new IllegalArgumentException("login non valide");

		try {
			Client.addClient(new Adherent(nom));
		} catch (ErreurStatut e) {
			System.out.println(e.getMessage());
		}
		client.setNom(nom);
	}

	// gérer la connexion du client
	public static void connecter(Scanner sc) {
		System.out.println("donnez votre login : ");
		String nom = null;
		String typeClient = null;
		nom = sc.nextLine();
		System.out.println("quel type de client etes vous : ");
		System.out.println("1 - Adherent");
		System.out.println("2 - Membre personnel");
		Scanner scI = new Scanner(System.in);
		int type = scI.nextInt();
		if (type == 1)
			typeClient = "Adherent";
		else if (type == 2)
			typeClient = "MembrePersonnel";
		try {
			client.seConnecter(nom, typeClient);
			System.out.println("vous etes connecte en tant que " + client.getSonStat().toString());
		} catch (ErreurStatut e) {
			System.out.println(e.getMessage());
		}

	}

	// affiche tous les produits vente
	public static void afficheProdui() {
		System.out.println("\nLES PRODUITS EN VENTE");
		for (Produit p : listProduit()) {
			System.out.println(p.id + "  " + p.getPrix() + "€");
		}
		System.out.println();
	}

	public static void ajoutProd(Scanner sc) {
		System.out.println("donnez le nom du produit : ");
		String name = sc.nextLine();
		Produit p = null;
		try {
			p = nameToProd(name);
			client.ajoutProduit(p);
			System.out.println(p.getId());
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}

	}

	public static void affichePanier() {
		for (Produit p : client.getSonPanier().getContenu()) {
			System.out.println("\t" + p.getId() + "   " + p.getPrix() + "€");
		}
	}

	public static void imprimeTicket() {
		System.out.println("\n\tLAFAC.COM");
		affichePanier();
		System.out.println("\ttotal = " + client.getSonPanier().getTotalAvant() + "€");
		System.out.println("\ttotal a payer = " + client.getSonPanier().getTotal() + "€");
		System.out.println();
	}

	public static void afficheMenu() {
		System.out.println("------ 0 - Sortie");
		System.out.println("------ 1 - s'inscrire");
		System.out.println("------ 2 - Se Connecter");
		System.out.println("------ 3 - lister les produits en vente");
		System.out.println("------ 4 - ajouter un produit");
		System.out.println("------ 5 - payer ");
		System.out.println("------ 6 - Se Deconnecter");
		System.out.println("------ 7 - Voir le panier");
		System.out.println("------ 8 - revoir le menu");
	}

}
